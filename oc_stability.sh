  #!/bin/bash

echo "Do you want to run sensor monitoring every 5 second? will try spawn LXterminal and xfce4-terminal kill command with CTRL+C"
select yn in "yes" "no" ; do
    case $yn in
        yes ) lxterminal --working-directory=${PWD} -e for i in {1..10000} ; do sleep 5 && sh ./stability/sensors.sh & xfce4-terminal -working-directory=${PWD} -e for i in {1..10000} ; do sleep 5 && sh ./stability/sensors.sh; break ;;
        no ) break;;
    esac
done

sh ./overclocking/test.sh


