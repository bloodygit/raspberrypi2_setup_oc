#!/bin/bash

sudo apt-get install xfce4 xfce4-goodies -y
sudo apt-get remove lxappearance lxde lxde-* lxinput lxmenu-data lxpanel lxpolkit lxrandr lxsession* lxsession lxshortcut lxtask lxterminal -y
sudo apt-get install pistore -y
sudo apt-get autoremove -y
sudo apt-get autoclean -y

