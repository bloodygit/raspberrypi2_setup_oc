#!/bin/bash

echo "Do you want to write config.txt, change KBlayout to CH or replace LXDE with XFCE?" 
select yn in "Yes" "No"; do
    case $yn in
        Yes ) sh ./firststart.sh ; break;;
        No ) break;;
    esac
done

echo "Do you want to test system stability?" 
select yn in "Yes" "No"; do
    case $yn in
        Yes ) sh ./oc_stability.sh ; break;;
        No ) break;;
    esac
done

echo "Do you want to test network stability?" 
select yn in "Yes" "No"; do
    case $yn in
        Yes ) sh ./networktest.sh ; break;;
        No ) break;;
    esac
done
