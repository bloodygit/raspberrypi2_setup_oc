#/bin/bash

echo "run python prime, choose 10k - 15k or higher"
python mprime.py
echo "python prime sucessfull"

sudo apt-get install memtester -y 
echo "run memtester pkg @500MB on 4 loops"
memtester 500m 4
echo "memtester finished on 4loops for 500MB"

echo "run sdcard test, read 10x enitre SD, write just 1x 512MB to preserve SDcard life"
sh sdcard.sh
echo "sdcard tested" 

sudo apt-get install sysbench -y
echo "run sysbench, primes again up to 20k on 4cores"
sysbench --num-threads=4 --test=cpu --cpu-max-prime=20000 --validate run
echo "sysbench done"

chmod +x ./cpuburn-a53
echo "run cpuburn for 15 min or till 70 degree celsius is reached"
runtime="15 minute"
endtime=$(date -ud "$runtime" +%s)
while [[ $(date -u +%s) -le $endtime ]]
do
   vcgencmd measure_clock arm; vcgencmd measure_temp; sleep 10; done & ./cpuburn-a53
done

echo " cpuburn done, NO ERROR HANDLING!"
echo "ALL TESTS DONE; feeling stable now, might crash later..."
