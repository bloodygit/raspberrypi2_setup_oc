 #!/bin/bash

echo "testing Network speed and/or just stability"
select ync in "Speed+Stability" "Stability" "cancel"; do
    case $ync in
        Speed+Stability ) sh ./networktest/internetspeed.sh; break;;
        Stability ) sh ./networktest/internetstability.sh; break;;
	cancel ) break;;
    esac
done
