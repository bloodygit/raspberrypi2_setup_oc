#!/bin/bash

echo "want to overclock?"

select yn in "Yes" "No"; do
    case $yn in
        Yes ) cp ./overclocking/config.txt /boot/config.txt; break;;
        No ) break;;
    esac
done

echo "change KBlayout to CH" 
sh ./setup/keyboardlayoutCH.sh

echo "replace LXDE with XFCE incl. pistore?"
select yn in "Yes" "No"; do
    case $yn in
        Yes ) sh ./setup/lxdexfce.sh;;
        No ) break;;
    esac
done


echo "need to reboot for OC / KB and/or DE change"

select yn in "reboot" "No"; do
    case $yn in
        Reboot ) reboot; break ;;
        No ) break;;
    esac
done
